const { app, BrowserWindow } = require('electron');
const path = require('path');
const fs = require("fs");

if (require('electron-squirrel-startup')) {
  app.quit();
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 400,
    height: 850,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {

  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

///Мое приложение экспорты
let bace = 10;

 // let Content = fs.readFileSync("A.txt", "utf8");


//export {bace};   //тут ошибка
module.exports = {bace};  //тут ошибка?

//document.getElementById('val_bace').value = Content;














